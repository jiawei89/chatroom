/*************************************************************************
	> File Name: client/client.c
	> Author: David.deng
	> Mail: 331470234@qq.com
	> Created Time: Fri 24 Dec 2021 02:04:51 PM CST
 ************************************************************************/

#include "../common/head.h"
const char *conf = "./wechat.conf";
void *client_work(void *arg);
int sockfd;
char name[20];
WINDOW *msg_win, *sub_msg_win, *info_win, *sub_info_win, *input_win, *sub_input_win;
void logout(int signum) {
    printf("close by sig, sockfd: %d\n", sockfd);
    struct wechat_msg msg;
    msg.type = WECHAT_FIN;
    strcpy(msg.name, name);
    if (send(sockfd, &msg, sizeof(msg), 0) == -1) {
        perror("send logout");
    }
    exit(0);
}
int main(int argc, char **argv) {
    int opt, server_port = -1, sex = -1, mode = 0;
    char server_ip[20] = {0}, passwd[20] = {0};
    while ((opt = getopt(argc, argv, "h:p:s:n:P:m:")) != -1) {
        switch (opt) {
            case 'h':
                strcpy(server_ip, optarg);
                break;
            case 'p':
                server_port = atoi(optarg); 
                break;
            case 's':
                sex = atoi(optarg);
                break;
            case 'n':
                strcpy(name, optarg);
                break;
            case 'P':
                strcpy(passwd, optarg);
                break;
            case 'm':
                mode = atoi(optarg);
                break;
            default:
                fprintf(stderr, "Usage : %s -p port -P passwd -n name -h host -s sex.\n", argv[0]);
                exit(1);
        }
    }

    if (!strlen(name)) strcpy(name, get_conf_value(conf, "NAME"));
    if (!strlen(passwd)) strcpy(passwd, get_conf_value(conf, "PASSWD"));
    if (!strlen(server_ip)) strcpy(server_ip, get_conf_value(conf, "SERVERIP"));
    if (server_port < 0) server_port = atoi( get_conf_value(conf, "PORT"));
    if (sex < 0) sex = atoi(get_conf_value(conf, "SEX"));

    if ((sockfd = socket_connect(server_ip, server_port)) < 0) {
        perror("socket_connect");
        exit(1);
    }

    #ifdef UI
    init_ui();
    init_info(name);
    #else
    DBG(BLUE"<D>"NONE" : name = %s.\n", name);
    DBG(BLUE"<D>"NONE" : passwd = %s.\n", passwd);
    DBG(BLUE"<D>"NONE" : server_ip = %s.\n", server_ip);
    DBG(BLUE"<D>"NONE" : server_port = %d.\n", server_port);
    DBG(BLUE"<D>"NONE" : sex = %s.\n", sex ? "男" :"女");
    DBG(BLUE"<D>"NONE" : connected to server with sockfd %d.\n", sockfd);
    #endif
    
    struct wechat_msg msg;
    bzero(&msg, sizeof(msg));
    strcpy(msg.name, name);
    strcpy(msg.passwd, passwd);
    msg.sex = sex;

    if (!mode) {
        #ifndef UI
        DBG(BLUE"<D>"NONE" : Program running in signup mode\n");
        #endif
        msg.type = WECHAT_SIGNUP;
    } else {
        #ifndef UI
        DBG(BLUE"<D>"NONE" : Program running in signin mode\n");
        #endif
        msg.type = WECHAT_SIGNIN;
    }
    send(sockfd, (void *)&msg, sizeof(msg), 0);

    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(sockfd, &rfds);
    struct timeval tv;
    tv.tv_sec = 2;
    tv.tv_usec = 0;
    if (select(sockfd + 1, &rfds, NULL, NULL, &tv) <= 0) {
        fprintf(stderr, RED"<System>"BLUE"server is out of service.\n"NONE);
        exit(1);
    }
    int ret = recv(sockfd, (void *)&msg, sizeof(msg), 0);
    if (ret <= 0) {
        fprintf(stderr, RED"<System>"BLUE"Something is wrong with server.\n"NONE);
        exit(1);
    }
    if (msg.type & WECHAT_ACK) {
        #ifndef UI
        DBG(GREEN"<Success>"NONE" : Server return a success.\n");
        #endif
        if (!mode) {
            #ifndef UI
            DBG(CYAN"Please run again.\n"NONE);
            #endif
            exit(0);
        }
    } else {
        #ifndef UI
        DBG(RED"<Failure>"NONE" : Server return a failure");
        #endif
        exit(1);
    }

    pthread_t tid;
    pthread_create(&tid, NULL, client_work, (void *)&sockfd);

        #ifndef UI
        DBG(BLUE"<D> sockfd: %d\n"NONE, sockfd);
        #endif
        signal(SIGINT, logout);

    while (1) {
        memset(msg.msg, 0, sizeof(msg.msg));
        #ifdef UI
        echo();
        nocbreak();
        mvwscanw(input_win, 1, 1, "%[^\n]s", msg.msg);
        #else
        memset(msg.msg, 0, sizeof(msg.msg));
        scanf("%[^\n]s", msg.msg);
        getchar();
        #endif
        msg.type = WECHAT_WALL;
        if (msg.msg[0] == '@') {
            msg.type = WECHAT_MSG;
            memset(msg.to, 0, sizeof(msg.to));
            strncpy(msg.to, msg.msg + 1, strchr(msg.msg, ' ') - msg.msg - 1); //@abcd hello
        }
        if (!strlen(msg.msg)) continue;
        send(sockfd, (void *)&msg,  sizeof(msg), 0);
        wclear(input_win);
        box(input_win, 0, 0);
        noecho();
        cbreak();
    }
    return 0;

}





                

