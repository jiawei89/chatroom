/*************************************************************************
	> File Name: wechat_client.h
	> Author: David.deng
	> Mail: 331470234@qq.com
	> Created Time: Sun 26 Dec 2021 07:55:15 PM CST
 ************************************************************************/

#ifndef _WECHAT_CLIENT_H
#define _WECHAT_CLIENT_H
void *client_work(void *arg);
#endif
