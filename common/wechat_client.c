/*************************************************************************
	> File Name: wechat_client.c
	> Author: David.deng
	> Mail: 331470234@qq.com
	> Created Time: Sun 26 Dec 2021 07:55:58 PM CST
 ************************************************************************/

#include "head.h"
extern WINDOW *msg_win, *sub_msg_win, *info_win, *sub_info_win, *input_win, *sub_input_win;
void *client_work(void *arg) {
    int fd = *(int*)arg;
    struct wechat_msg msg;
    while (1) {
        memset(&msg, 0, sizeof(msg));
        int ret = recv(fd, &msg, sizeof(msg), 0);
        if (ret <= 0) {
            fprintf(stderr, RED"<Error"NONE" : Server is out service.\n");
            exit(1);
        }
        if (msg.type & WECHAT_WALL) {
            #ifdef UI
            show_msg(&msg);
            #else
            DBG(BLUE"%s"NONE": %s\n", msg.name, msg.msg);
            #endif
        } else if (msg.type & WECHAT_MSG) {
            #ifdef UI
            show_msg(&msg);
            #else
            DBG(GREEN"*%s"NONE": %s\n", msg.name, msg.msg);
            #endif
        } else if (msg.type & WECHAT_SYS) {
            #ifdef UI
            show_msg(&msg);
            #else
            DBG(PINK"Server"NONE": %s\n", msg.msg);
            #endif
        }
    }
}

