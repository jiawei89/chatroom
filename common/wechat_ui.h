/*************************************************************************
	> File Name: wechat_ui.h
	> Author: David.deng
	> Mail: 331470234@qq.com
	> Created Time: Sun 26 Dec 2021 08:14:30 PM CST
 ************************************************************************/

#ifndef _WECHAT_UI_H
#define _WECHAT_UI_H
#define MSG_WIDTH 80
#define MSG_HEIGHT 20
#define INFO_WIDTH 15
void init_ui();
void gotoxy_puts(int x, int y, char *s);
void gotoxy_putc(int x, int y, char c);
void gotoxy(int x, int y);
void w_gotoxy_puts(WINDOW *win, int x, int y, char *s);
void w_gotoxy_putc(WINDOW *win, int x, int y, char c);
void destroy_win(WINDOW *win);
WINDOW *create_newwin(int width, int height, int x, int y);
void show_msg(struct wechat_msg *msg);
void init_info(char *name);
#endif
