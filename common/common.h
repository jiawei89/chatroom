/*************************************************************************
	> File Name: common.h
	> Author: David.deng
	> Mail: 331470234@qq.com
	> Created Time: Mon 06 Dec 2021 09:11:59 AM CST
 ************************************************************************/

#ifndef _COMMON_H
#define _COMMON_H
char conf_ans[512];
int socket_create(int port);
int socket_connect(const char *ip, int port);
int make_block(int fd);
int make_nonblock(int fd);
char *get_conf_value(const char *filename, const char *key);
#endif
