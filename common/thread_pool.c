/*************************************************************************
	> File Name: thread_pool.c
	> Author: David.deng
	> Mail: 331470234@qq.com
	> Created Time: Mon 06 Dec 2021 09:44:21 AM CST
 ************************************************************************/

#include "thread_pool.h"

extern char *data[2000];
extern int epollfd;
extern pthread_mutex_t mutex[2000];

void task_queue_init(struct task_queue *taskQueue, int size) {
    taskQueue->size = size;
    taskQueue->total = taskQueue->head = taskQueue->tail = 0;
    taskQueue->data = calloc(size, sizeof(void *));
    pthread_mutex_init(&taskQueue->mutex, NULL);
    pthread_cond_init(&taskQueue->cond, NULL);
    return ;
}
void task_queue_push(struct task_queue *taskQueue, void *data) {
    pthread_mutex_lock(&taskQueue->mutex);
    if (taskQueue->total == taskQueue->size) {
        DBG(YELLOW"<push> taskQueue is full.\n"NONE);
        pthread_mutex_unlock(&taskQueue->mutex);
        return ;
    }
    taskQueue->data[taskQueue->tail] = data;
    DBG(PINK"<push> push to %dth task.\n"NONE, taskQueue->tail);
    taskQueue->total++;
    if (++taskQueue->tail == taskQueue->size) {
        DBG(PINK"<push> tail begins with 0.\n"NONE);
        taskQueue->tail = 0;
    }
    pthread_cond_signal(&taskQueue->cond);
    pthread_mutex_unlock(&taskQueue->mutex);
}

void* task_queue_pop(struct task_queue *taskQueue) {
    pthread_mutex_lock(&taskQueue->mutex);
    while (taskQueue->total == 0) {
        pthread_cond_wait(&taskQueue->cond, &taskQueue->mutex);
    }
    void *data = taskQueue->data[taskQueue->head];
    DBG(BLUE"<pop> pop data from %dth task.\n"NONE, taskQueue->head);
    taskQueue->total--;
    if (++taskQueue->head == taskQueue->size) {
        taskQueue->head = 0;
    }
    pthread_mutex_unlock(&taskQueue->mutex);
    return data;
}
void* thread_run(void *arg) {
    pthread_detach(pthread_self());
    struct task_queue *taskQueue = (struct task_queue *)arg;
    while (1) {
        void *data = task_queue_pop(taskQueue);
        DBG(GREEN"<thread> got a task!\n"NONE);
    }
    return (void*)0;
}

void do_work(int fd) {
    char buff[4096] = {0};
    int rsize = -1;
    int ind = strlen(data[fd]);
    if ((rsize = recv(fd, buff, sizeof(buff), 0)) <= 0) {
        epoll_ctl(epollfd, EPOLL_CTL_DEL, fd, NULL);
        DBG(RED"<Close> : connection closed by peer!\n"NONE);
        close(fd);
        return ;
    } 
    //pthread_mutex_lock(&mutex[fd]);
    DBG(YELLOW"<Recv> : recv from clients.\n"NONE);
    for (int i = 0; i < rsize; i++) {
        if (buff[i] >= 'A' && buff[i] <= 'Z') {
            data[fd][ind++] = buff[i] + 32;
        } else if (buff[i] >= 'a' && buff[i] <= 'z') {
            data[fd][ind++] = buff[i] - 32;
        } else {
            data[fd][ind++] = buff[i];
            if (buff[i] == '\n') {
                DBG(GREEN"<succes> send data to clients!\n"NONE);
                send(fd, data[fd], ind, 0);
                ind = 0; 
                memset(data[fd], 0, sizeof(data[fd]));
            }
        }
    }
    /*
    struct epoll_event ev;
    ev.data.fd = fd;
    ev.events = EPOLLIN | EPOLLET;
    make_nonblock(fd);
    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) < 0) {
        perror("epoll_ctl again ");
        exit(1);
    }
    */
    //pthread_mutex_unlock(&mutex[fd]);

}
void* thread_work(void *arg) {
    pthread_detach(pthread_self());
    struct task_queue *taskQueue = (struct task_queue *)arg;
    while (1) {
        int *fd = task_queue_pop(taskQueue);
        DBG(GREEN"<thread> got a task!\n"NONE);
        do_work(*fd);
    }
    return (void*)0;
}
