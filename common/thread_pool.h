/*************************************************************************
	> File Name: thread_pool.h
	> Author: David.deng
	> Mail: 331470234@qq.com
	> Created Time: Mon 06 Dec 2021 09:27:45 AM CST
 ************************************************************************/
#ifndef _THREAD_POOL_H
#define _THREAD_POOL_H
#include "head.h"
struct task_queue {
    int head, tail, size, total;
    void **data;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
};

void task_queue_init(struct task_queue *taskQueue, int size);
void task_queue_push(struct task_queue *taskQueue, void *data);
void* task_queue_pop(struct task_queue *taskQueue);
void* thread_run(void *arg);
void* thread_work(void *arg);
#endif
