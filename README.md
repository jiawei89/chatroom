# Project description

Chat room design model based on CS architecture. Using multithreading and IO multiplexing technology, the master-slave reactor mode can be realized, and people of different genders can be assigned to different chat rooms.

server： 
![sever](./Res/1.jpg)

client with cmd: 
![](./Res/2.jpg)

client with ui: 
![](./Res/3.jpg)

# How to Build

use makefile from client and server folder

# How to use

### run server

```
./server -p 8888
```

### regist

```
./cmd_client -m0
```

### run client

```
./cmd_client -m1 -n yourname -P yourpassword
```

### run client with ui

```
./ui_client -m1 -n yourname -P yourpassword
```
